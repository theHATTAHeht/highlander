# ------------------------------
# Written by Highlander 
# MedNetwoRx Systems Scripts
#
# DICOM Mass PDF Converter
# ------------------------------


# Declare Statements
ft="image/jpeg; charset=binary"

# ss is 'Split Size'
ss=5000
# The default value of the split command I have built is 5000 and was based on the size of job.
# If your directory is very large you may need to increase this number as it will spawn too many processes.
# Likewise if the directory is small you need to reduce this number to speed up the run speed.


# cf is 'Convert File'
cf="convert.list"
# This is the file the script will look for to run the commands against in the parent dir.
# I have built the script to be flexible on this name, but it lines up to other version of this command
# so expect to have to change this every time I release an update to this script.

echo "========================================"
echo "|       DICOM Mass PDF Converter       |"
echo "|           by MedNetwoRx              |"
echo "========================================"
echo " "
echo " "
echo "This app search the active directory and sub-directories for DCM files"
echo "and then convert them to PDF.  Any errors will be stored in the Conversion.Report"
echo " "
read -rsp $'Press any key or wait 5 seconds to continue...\n' -n 1 -t 5;


# If statement to see if a previous Find command or existing convert.list file exists.
# If you have a manually compiled list you can put it in the parent direcotry of the DCM files and run.
# This statement should see it and skip the Discovery step.
#
# Note: This step can take a while so be patient.


	if [ ! -e "$cf" ]; then
		echo "No convert list found, running discovery..."
		echo "."
		find . -name "*.dcm" -type f > $cf
		echo "."
		echo "Discovery completed."
	#read -rsp $'Press any key or wait 5 seconds to continue...\n' -n 1 -t 5;
	fi

# To handle the jobs more efficiently I have used a split command to chunk the lists of files.	

	echo "$cf found, splitting file..."
	echo "."
split -l $ss -d $cf ConvSeg
	echo "."
	echo "File Split."
wait
	#read -rsp $'Press any key or wait 5 seconds to continue...\n' -n 1 -t 5;

# Each list chunk will spawn a seperate loop via distributed processing and export its results to a log.

	echo "Starting loop..."
for i in $(find . -name "ConvSeg*" -type f); do
	while read a; do
		dcm2pdf -f -ui +uc +ae -ll debug "$a" "${a%.dcm}.pdf" |& tee -a "$i.log"
		echo "$a Converted" |& tee -a "$i.log"
	done < $i &
	#read -rsp $'Press any key or wait 5 seconds to continue...\n' -n 1 -t 5;
wait
done

	echo "Processing completed."
	echo "Cleaning up..."
	echo "."
echo "##############################################################" >> Conversion.Report
echo "################# Beginning DCM Conversion ###################" >> Conversion.Report
echo "##############################################################" >> Conversion.Report

cat *.log >> Conversion.Report
rm ConvSeg*
rm $cf
	echo "."
	echo "Logs compressed."
	echo "Segments deleted."
	echo "$cf deleted."

#Added a utility to clean up bad image types

#echo "========================================"
#echo "|    DICOM Mass PDF Cleanup Utility    |"
#echo "|           by MedNetwoRx              |"
#echo "========================================"
#echo " "
#echo " "
#echo "This app search the active directory and sub-directories for PDF files"
#echo "and then convert them to JPG if necessary.  Any errors will be stored" 
#echo "in the Conversion.Report"
#echo " "
#read -rsp $'Press any key or wait 5 seconds to continue...\n' -n 1 -t 5;


# If statement to see if a previous Find command or existing convert.list file exists.
# If you have a manually compiled list you can put it in the parent direcotry of the DCM files and run.
# This statement should see it and skip the Discovery step.
#
# Note: This step can take a while so be patient.


	if [ ! -e "$cf" ]; then
		echo "No convert list found, running discovery..."
		echo "."
		find . -name "*.pdf" -type f > $cf
		echo "."
		echo "Discovery completed."
	#read -rsp $'Press any key or wait 5 seconds to continue...\n' -n 1 -t 5;
	fi

# To handle the jobs more efficiently I have used a split command to chunk the lists of files.	

	echo "$cf found, splitting file..."
	echo "."
split -l $ss -d $cf ConvSeg
	echo "."
	echo "File Split."
wait
	#read -rsp $'Press any key or wait 5 seconds to continue...\n' -n 1 -t 5;

# Each list chunk will spawn a seperate loop via distributed processing and export its results to a log.

	echo "Starting loop..."
for i in $(find . -name "ConvSeg*" -type f); do
	while read a; do
		file -i $a |& tee -a "$i.log"
		cf=$(file -ib $a)
		if [ "$cf" == "$ft" ]; then
		    mv "$a" "${a%.pdf}.jpg" 
			echo "$a Converted to JPG ##############################################################" |& tee -a "$i.log"
		else echo "$a is not a jpg file"     
		fi
		
	done < $i &
	#read -rsp $'Press any key or wait 5 seconds to continue...\n' -n 1 -t 5;
wait
done

	echo "Processing completed."
	echo "Cleaning up..."
	echo "."

echo "##############################################################" >> Conversion.Report
echo "################# Beginning Image Conversion #################" >> Conversion.Report
echo "##############################################################" >> Conversion.Report
cat *.log >> Conversion.Report
rm ConvSeg*
rm $cf
	echo "."
	echo "Logs compressed."
	echo "Segments deleted."
	echo "$cf deleted."
