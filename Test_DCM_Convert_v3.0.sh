cf="convert.list"

echo "========================================"
echo "|       DICOM Mass PDF Converter       |"
echo "|           by Highlander              |"
echo "========================================"
echo " "
echo " "
echo "This app search the active directory and sub-directories for DCM files"
echo "and then convert them to PDF.  Any errors will be stored in the convesion.log"
echo " "
read -rsp $'Press any key or wait 5 seconds to continue...\n' -n 1 -t 5;

	if [ ! -e "$cf" ]; then
		echo "No convert list found, running discovery..."
		echo "."
		find . -name "*.dcm" -type f > $cf
		echo "."
		echo "Discovery completed."

	fi

	echo "$cf found, splitting file..."
	echo "."
split -l 5 -d $cf ConvSeg
	echo "."
	echo "File Split."
wait
	echo "Starting loop..."
for i in $(find . -name "ConvSeg*" -type f); do
	while read a; do
		dcm2pdf -f -ui +uc +ae -ll debug "$a" "${a%.dcm}.pdf" | tee -a Conversion.log
		echo "$a Converted"
	done < $i &
wait
done

	echo "Processing completed."
	echo "Cleaning up..."
	echo "."
#cat log* > Conversion.log
rm ConvSeg*
rm log*
rm $cf
	echo "."
	echo "Logs compressed."
	echo "Segments deleted."
	echo "$cf deleted."